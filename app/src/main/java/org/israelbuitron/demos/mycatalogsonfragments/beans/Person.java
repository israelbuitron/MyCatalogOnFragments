package org.israelbuitron.demos.mycatalogsonfragments.beans;

public class Person {

    private String mName;
    private String mAge;

    public Person(String name, String age) {
        mName = name;
        mAge = age;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getAge() {
        return mAge;
    }

    @SuppressWarnings("unused")
    public void setAge(String age) {
        mAge = age;
    }

    @Override
    public String toString() {
        return "name: " + mName + ", age: " + mAge;
    }
}
