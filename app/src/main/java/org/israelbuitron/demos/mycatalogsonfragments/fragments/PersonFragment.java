package org.israelbuitron.demos.mycatalogsonfragments.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.israelbuitron.demos.mycatalogsonfragments.R;
import org.israelbuitron.demos.mycatalogsonfragments.adapters.PersonRecyclerViewAdapter;
import org.israelbuitron.demos.mycatalogsonfragments.beans.Person;
import org.israelbuitron.demos.mycatalogsonfragments.dummy.DummyContent;

public class PersonFragment extends Fragment {

    // Argument name for column count
    private static final String ARG_COLUMN_COUNT = "column-count";

    // Column count for fragment. Default is 1.
    private int mColumnCount = 1;

    private OnListFragmentInteractionListener mListener;

    public PersonFragment() {
        // Required empty public constructor
    }

    @SuppressWarnings("unused")
    public static PersonFragment newInstance(int columnCount) {
        // Create fragment
        PersonFragment fragment = new PersonFragment();

        // Pack parameters for fragment
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_person_list, container, false);

        // Configure adapter
        if (view instanceof RecyclerView) {
            // Getting context
            Context context = view.getContext();

            // Getting view
            RecyclerView recyclerView = (RecyclerView) view;

            // Configure view using column count
            if (mColumnCount <= 1) {
                // Use a linear layout when one column
                recyclerView.setLayoutManager(new LinearLayoutManager(context));
            } else {
                // Use a grid layout when more than one column
                recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
            }

            // Set adapter for view
            RecyclerView.Adapter adapter = new PersonRecyclerViewAdapter(DummyContent.PEOPLE, mListener);
            recyclerView.setAdapter(adapter);
        }
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {
        void onListFragmentInteraction(Person person);
    }
}
