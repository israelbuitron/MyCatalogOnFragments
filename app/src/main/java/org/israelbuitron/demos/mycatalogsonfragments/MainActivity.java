package org.israelbuitron.demos.mycatalogsonfragments;

import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import org.israelbuitron.demos.mycatalogsonfragments.beans.Person;
import org.israelbuitron.demos.mycatalogsonfragments.fragments.PersonFormFragment;
import org.israelbuitron.demos.mycatalogsonfragments.fragments.PersonFragment;

public class MainActivity extends AppCompatActivity
        implements PersonFormFragment.OnFragmentInteractionListener,
        PersonFragment.OnListFragmentInteractionListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_main);

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Configuration c = getResources().getConfiguration();

        if (c.orientation == Configuration.ORIENTATION_PORTRAIT) {
            ft.replace(android.R.id.content, PersonFormFragment.newInstance("Israel","30"));
            Toast.makeText(this, "Switching to Form", Toast.LENGTH_SHORT).show();
        } else if (c.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            ft.replace(android.R.id.content, PersonFragment.newInstance(1));
            Toast.makeText(this, "Switching to List", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Unknown orientation", Toast.LENGTH_SHORT).show();
        }

        ft.commit();
    }

    @Override
    public void onFragmentInteraction(Uri uri) {
        Toast.makeText(this, "URI" + uri, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onListFragmentInteraction(Person person) {
        Toast.makeText(this, "Person: " + person, Toast.LENGTH_SHORT).show();
    }
}
